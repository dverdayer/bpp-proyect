/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ppp.client.util;

import com.frm.proto.client.salpi.BaseModelData;
import com.google.gwt.user.client.rpc.IsSerializable;
import java.util.List;

public class FDFormFree extends FormDinamicoAgr implements IsSerializable{
    private Integer nlKey;
    private String estReg;
    private String strLabel;
    private List<BaseModelData> lstOpc;
    private String ssTipoAgr;

    public FDFormFree() {
    }

    public FDFormFree(String strLabel, List<BaseModelData> lstOpc, String ssTipoAgr, Integer nlKey, String ssLabelTop ) {
        this.strLabel = strLabel;
        this.lstOpc = lstOpc;
        this.ssTipoAgr = ssTipoAgr;
        this.nlKey = nlKey;
        super.setSsLabelTop(ssLabelTop);
    }

    public String getSsTipoAgr() {
        return ssTipoAgr;
    }

    public void setSsTipoAgr(String ssTipoAgr) {
        this.ssTipoAgr = ssTipoAgr;
    }

    public List<BaseModelData> getLstOpc() {
        return lstOpc;
    }

    public void setLstOpc(List<BaseModelData> lstOpc) {
        this.lstOpc = lstOpc;
    }

    public String getStrLabel() {
        return strLabel;
    }

    public void setStrLabel(String strLabel) {
        this.strLabel = strLabel;
    }

    public Integer getNlKey() {
        return nlKey;
    }

    public void setNlKey(Integer nlKey) {
        this.nlKey = nlKey;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }


}
