/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ppp.client.util;

import com.google.gwt.user.client.rpc.IsSerializable;
/**
 *
 * @author diegohernandez
 */
public class FormDinamicoAgr  implements IsSerializable{
    private Integer nlOrden;
    private String ssLabelTop;

    public Integer getNlOrden() {
        return nlOrden;
    }

    public void setNlOrden(Integer nlOrden) {
        this.nlOrden = nlOrden;
    }

    public String getSsLabelTop() {
        return ssLabelTop;
    }

    public void setSsLabelTop(String ssLabelTop) {
        this.ssLabelTop = ssLabelTop;
    }

}
