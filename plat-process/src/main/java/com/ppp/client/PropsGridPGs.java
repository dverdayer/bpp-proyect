/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ppp.client;

import com.frm.proto.client.salpi.BaseModelData;
import com.frm.proto.client.salpi.PUN;
import com.frm.proto.client.salpi.Pareto;
import com.frm.proto.client.salpi.VentanaLoad;
import com.frm.proto.client.util.PropsGridPG;
import com.frm.proto.client.util.Recurso;

/**
 *
 * @author diego
 */
public final class PropsGridPGs {
    
    public static PropsGridPG getPropsGridPG(Recurso r){

        if(r.getSsXml().equals("RECURSOS")){
            
            r.setSsEntidad("AdmRecursos");
            
            String[] idsGrid = {"kaNlRecurso","ssRecurso","nlOrden"
                            ,"admModulos:kaNlModulo","admModulos:ssDescripcion"
                            ,"admRecursosp:kaNlRecurso","admRecursosp:ssRecurso"
                            ,"ssXml","ssVariable"
                            ,"ssNombre","ssTipoVentana"
                            ,"ssBorrado"
            };

            String[] idsfilterGrid = {"ssRecurso","admModulos:kaNlModulo","admModulos:ssDescripcion"
                                ,"admRecursosp:kaNlRecurso","admRecursosp:ssRecurso"
                                ,"ssXml","ssVariable"
            };

            String[] titulosGrid = {"....","Descripcion","Orden"
                                    ,"...","Aplicacion"
                                    ,"...","Padre"
                                    ,"XML","variable"
                                    ,"Nombre","Tipo Ventana"
                                    ,"Borrado"
            };      


            String[] tiposCampoGrid = {PUN.HDD_NU,PUN.TXT_NN,PUN.TXN_NN_D0
                        ,PUN.HDD_NU, PUN.PPP_NN
//                        ,PUN.HDD_NU, PUN.CBE_XX
                        ,PUN.HDD_NU, PUN.ENTIDADE_NN
                        ,PUN.TXT_NN, PUN.TXT_NN
                        ,PUN.TXT_NN, PUN.CBX_NN+"_TIPOVENTANA"
                        ,PUN.CHK_NN
            };

            String[] strGrid = {PUN.SMALL, PUN.SMALL,PUN.SMALL
                    ,PUN.SMALL,PUN.SMALL
                    ,PUN.SMALL,PUN.SMALL
                    ,PUN.SMALL,PUN.SMALL
                    ,PUN.SMALL,PUN.SMALL
                    ,PUN.SMALL
            };        
            boolean[] trigger = {false,false,false
                        ,false,false
                        ,false,false
                        ,false,false
                        ,false,false
                        ,false
            };
            boolean[] editables = {true,true,true
                    ,true,true
                    ,true,true
                    ,true,true
                    ,true,true
                    ,true
            };

            BaseModelData bmdVL = new BaseModelData();               
            VentanaLoad vlFormTer = getModulos(r.getSsControlador());
            bmdVL.set("admModulos:ssDescripcion", vlFormTer);

            BaseModelData paretosBorrar = new BaseModelData();
            paretosBorrar.set("admModulos:ssDescripcion", "admRecursosp:ssRecurso");

            final PropsGridPG pgpg = new PropsGridPG(r, r.getSsEntidad(), idsGrid, titulosGrid, tiposCampoGrid
                        , trigger, editables, strGrid, bmdVL, paretosBorrar);
            return pgpg;
        }else if(r.getSsXml().equals("MODULOS")){
            
            r.setSsEntidad("AdmModulos");
            String[] ids = {"kaNlModulo","ssDescripcion","ssDirUrl","ssMultiempresa","ssBorrado"};
            String[] titulos = {"kaNlModulo","Descripcion","Direcion URL","Multiempresa","Activo"};
            String[] tiposCampoGrid = {PUN.HDD_NU,PUN.TXT_NN,PUN.TXT_NN, PUN.CHK_NN, PUN.CHK_NN};
            String[] strGrid = {PUN.SMALL, PUN.SMALL,PUN.SMALL,PUN.SMALL,PUN.SMALL};                            
            boolean [] editables = {false,true,true,true,true};
            boolean [] trigger = {false,false,false,false,false};
            
            final PropsGridPG pgpg = new PropsGridPG(r, r.getSsEntidad(), ids, titulos, tiposCampoGrid
                        , trigger, editables, strGrid, null, null);
            return pgpg;
        }else{
            /// error no se encontro ningun recurso para la entidad
            return null;
        }
    }
    
    
    public static VentanaLoad getVLTiposUsuario(String ssControlador){
        String[] idsRecur = {"kaNlTiposUsuario","ssDescripcion"};
        String[] filter = {"ssDescripcion"};
        String[] titulosRecur = {"....","Descripcion"};
        String[] tipoCampoRecur = {PUN.HDD_NU,PUN.TXT_NN};       
        
        Pareto[] vtPareto = {new Pareto("admTiposUsuario:kaNlTiposUsuario", "kaNlTiposUsuario")
                             ,new Pareto("admTiposUsuario:ssDescripcion", "ssDescripcion")};
        
        VentanaLoad vl = new VentanaLoad("admTiposUsuario", ssControlador, idsRecur
                , titulosRecur, tipoCampoRecur, filter,vtPareto);
        return vl;
    }
    
    public static VentanaLoad getModulos(String ssControlador){

        String[] idsRecur = {"kaNlModulo", "ssDescripcion", "ssBorrado", "ssMultiempresa", "ssDirUrl"};
        String[] filter = {"kaNlModulo", "ssDescripcion", "ssBorrado", "ssMultiempresa", "ssDirUrl"
                            };
        String[] titulosRecur = {"kaNlModulo", "ssDescripcion", "ssBorrado", "ssMultiempresa", "ssDirUrl"};
        
        String[] tipoCampoRecur = {PUN.HDD_NU,PUN.TXT_NN,PUN.TXT_NN,PUN.TXT_NN,PUN.TXT_NN                    
            };        
        Pareto[] vtPareto = {new Pareto("admModulos:kaNlModulo" , "kaNlModulo")
                        ,new Pareto("admModulos:ssDescripcion","ssDescripcion")
            };

        VentanaLoad vl = new VentanaLoad("admModulos", ssControlador, idsRecur
                , titulosRecur, tipoCampoRecur, filter, vtPareto);
        return vl;

    }       
    
    public static VentanaLoad getTeceros(String ssControlador){

        String[] idsRecur = {"kaNl","ssPrimerApellido","ssSegundoApellido","ssPrimerNombre","ssSegundoNombre"
                            ,"ssCedula","ssEmail","ssCelular"
                            ,"crTipos:kaNlTipo","crTipos:ssTipo"};
        String[] filter = {"ssPrimerApellido","ssPrimerNombre","ssCedula","ssEmail","ssCelular"
                            };
        String[] titulosRecur = {"kaNl","Primer Apellido","Segundo Apellido","Primer Nombre","Segundo Nombre"
                            ,"Identificacion","E-mail","# Celular"
                            ,"crTipos:kaNlTipo","Tipo Tercero"};
        
        String[] tipoCampoRecur = {PUN.HDD_NU,PUN.TXT_NN,PUN.TXT_NN,PUN.TXT_NN,PUN.TXT_NN
                    ,PUN.TXT_NN,PUN.TXT_XX,PUN.TXT_XX
                    ,PUN.HDD_NU,PUN.ENTIDAD_NN
            };        
        Pareto[] vtPareto = {new Pareto("crTerceros:kaNl" , "kaNl")
                        ,new Pareto("crTerceros:ssPrimerNombre","ssPrimerNombre")
                        ,new Pareto("crTerceros:ssSegundoNombre" ,"ssSegundoNombre")
                        ,new Pareto("crTerceros:ssPrimerApellido","ssPrimerApellido")
                        ,new Pareto("crTerceros:ssSegundoApellido","ssSegundoApellido")
                        ,new Pareto("crTerceros:ssCedula","ssCedula")
                        ,new Pareto("crTerceros:ssCelular","ssCelular")
                        ,new Pareto("crTerceros:ssEmail","ssEmail")};

        VentanaLoad vl = new VentanaLoad("crTerceros", ssControlador, idsRecur
                , titulosRecur, tipoCampoRecur, filter, vtPareto);
        return vl;

    }
}
