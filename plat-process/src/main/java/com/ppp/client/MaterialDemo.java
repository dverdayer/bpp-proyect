package com.ppp.client;

import com.frm.proto.client.salpi.AdmUsuariosAplicaLigth;
import com.frm.proto.client.FormaGUI;
import com.frm.proto.client.GUI;
import com.frm.proto.client.ModelGeneric;
import com.frm.proto.client.salpi.PUN;
import com.frm.proto.client.RegObjClient;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
//import com.sodia.client.DTO.JsonBuilder;
//import com.sodia.client.Place;
import com.frm.proto.client.util.Recurso;
import com.frm.proto.client.widget.serv.JsonBuilder;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import gwt.material.design.client.events.SideNavPushEvent;
import gwt.material.design.client.ui.MaterialContainer;
import gwt.material.design.client.ui.MaterialNavBar;
import gwt.material.design.client.ui.MaterialNavBrand;
import gwt.material.design.client.ui.MaterialNavSection;
import gwt.material.design.client.ui.MaterialPanel;
import gwt.material.design.client.ui.MaterialSideNavPush;
import java.util.HashMap;
import java.util.Map;

public class MaterialDemo extends Composite {

    private static MaterialDemoUiBinder uiBinder = GWT.create(MaterialDemoUiBinder.class);
    private FormaGUI fguiActual;
    
    interface MaterialDemoUiBinder extends UiBinder<Widget, MaterialDemo> {
    }
    
    @UiField static MaterialContainer contentPanel;
    @UiField MaterialSideNavPush sideNav;
    @UiField MaterialNavBar navBar;      
    @UiField MaterialNavBrand barraSuperior;      
    @UiField MaterialNavSection navSection;

    private Map<Integer, Recurso> recursos;
    private static GUI GUIActual;


    public MaterialDemo() {
        initWidget(uiBinder.createAndBindUi(this));
        
        navBar.getElement().getStyle().setHeight(64, Style.Unit.PX);
        
        History.addValueChangeHandler(new ValueChangeHandler<String>() {
                @Override
                public void onValueChange(ValueChangeEvent<String> event) {
                        String token = event.getValue();
                        handleHistoryToken(token);
                }
        });                
        handleHistoryToken(History.getToken());                                                                                                                           
    
        sideNav.addHandler(new SideNavPushEvent.SideNavPushHandler() {
            @Override
            public void onPush(SideNavPushEvent event) {
//                int duration = event.getDuration();
                
//                Style style = navBar.getElement().getStyle();
//                style.setProperty("transition", duration + "ms");
//                style.setProperty("mozTransition", duration + "ms");
//                style.setProperty("webkitTransition", duration + "ms");

                 MaterialSideNavPush msnp = (MaterialSideNavPush)event.getSource();
//                 MaterialToast.fireToast("tamaño del contend : "+event.getElement().getId()+" .... "+msnp.getWidth()+" ... "+msnp.isOpen());
                 if(fguiActual != null && Window.getClientWidth() > 992){
                     if(msnp.isOpen()){
//                        fguiActual.getTrigger(event.getElement(), event.getElement().getId(), PUN.EVN_SID_NAV_OPEN);
                        fguiActual.getTrigger(event.getElement(), event.getElement().getId(), PUN.EVN_SID_NAV_OPEN);
                     }else{
//                        fguiActual.getTrigger(event.getElement(), event.getElement().getId(), PUN.EVN_SID_NAV_CLOSE);
                        fguiActual.getTrigger(event.getElement(), event.getElement().getId(), PUN.EVN_SID_NAV_CLOSE);
                     }
                 }
            }
        }, SideNavPushEvent.TYPE);                                
            
        ModelGeneric mg = new ModelGeneric();
        AdmUsuariosAplicaLigth aual = (AdmUsuariosAplicaLigth) RegObjClient.get(PUN.STR_USER_ACTUAL);
        mg.setCampoNormal(PUN.MODULO, String.valueOf(aual.getNlAplicacion()));                                                
        recursos = new HashMap<>();       
        JsonBuilder.refreshArbolNavigator(null, mg, sideNav, recursos);                              
        
        
//        MaterialLink ml  = new MaterialLink();
//        ml.setWaves(WavesType.LIGHT);
//        ml.setType(ButtonType.FLOATING);
//        ml.setIconType(IconType.RESTORE);
//        ml.setSize(ButtonSize.LARGE);
//        ml.setBackgroundColor(Color.BLUE_DARKEN_4);
//        
//        navSection.add(ml);
    }
  
        
    public Map<Integer, Recurso> getRecursos() {
        return recursos;
    }

    public void setRecursos(Map<Integer, Recurso> recursos) {
        this.recursos = recursos;
    }   
    
    
    /**
     * no es de este mundo pero miraremos si hay errores no comunes hay que borrarlo para saber que pasa
     * As materialize.js does not provide a init method we are calling displayEffect
     * directly. If Materialize ever change this function name we must change it here
     * as well.
     */
    private native void initWaves()/*-{
        $wnd.Waves.displayEffect();
    }-*/;

    public static GUI getGUIActual() {
        return GUIActual;
    }

    public static void setGUIActual(GUI GUIActual) {
        MaterialDemo.GUIActual = GUIActual;
    }
            
    private void handleHistoryToken(String token) {
        Place place = Place.inicio;
        if (!"".equals(token)) {
            if(token.split(PUN.SEPARA).length == 2 ){                
                Integer nlRecurso = Integer.valueOf(token.split(PUN.SEPARA)[1]);
                Recurso r = recursos.get(nlRecurso);          
                if(r.getSsTipoVentana() != null){// ventanas trans
                    if (r.getSsTipoVentana().equals(PUN.VENTANA_TIPO_T)){
                        place = Place.valueOf(r.getSsXml());
                        if(place != null){
                            place.setView(r);
                        }
                        changeNav(place);
                    }else if(r.getSsTipoVentana().equals(PUN.MAESTRO_TIPO_B)){
                        place = Place.valueOf(r.getSsTipoVentana());
                        if(place != null){
                            place.setView(r);
                        }
                        changeNav(place);
                    }else if(r.getSsTipoVentana().equals(PUN.VENTANA_TIPO_F)){
                        place = Place.valueOf(r.getSsXml());
                        if(place != null){
                            place.setView(r);
                        }
                        changeNav(place);
                    }                    
                }else{
                    /*tipo de ventana no implementada error en el recuros con el */
                }                
            }else{
                /**	
                 * aqui hacia abajo logica de las ventanas trasaccionales 
                 * algo asi como lo de la ventana trans
                 */
            }

        }else{ 
            changeNav(place);           
        }
    }   
    
    private void changeNav(Place place){
        if(place == null){
            place = Place.valueOf("incio"); 
        }                    
        if(contentPanel  != null){
            contentPanel.clear();
        }

        Widget w = place.getContent();
        if(w instanceof FormaGUI){
            
            String id = place.getView().getSsControlador()+":"+place.getView().getSsXml();
            String strModActual = PUN._PEST_TRANS1+id;
            RegObjClient.register(PUN.MODULO_ACTUAL,strModActual);
            
            fguiActual = (FormaGUI)w;
            barraSuperior.setText(place.getView().getSsRecurso());
            RegObjClient.unregisterByControlador(place.getView().getSsControlador());
            fguiActual.onRender();        
                        
            MaterialPanel nh = (MaterialPanel)contentPanel.getParent();                                    
//            MaterialToast.fireToast("tamaño del contend : " 
//                    + contentPanel.getElement().getClientHeight()+" ... "+nh.getOffsetHeight()+" ... "+Window.getClientHeight());
        }  
        if(contentPanel  != null){
            contentPanel.add(w);	        
        }        
        sideNav.hide();               
    }
    
}