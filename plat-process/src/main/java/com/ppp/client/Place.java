package com.ppp.client;


import com.frm.proto.client.util.Recurso;
import com.google.gwt.user.client.ui.Widget;
import com.ppp.client.GUI.GUIFormularios;
import com.ppp.client.GUI.GUISecond;
import com.ppp.client.GUI.GUIMaestroTipoB2;
import com.ppp.client.GUI.GUIPerfiles;
import com.ppp.client.GUI.GUIUsuarios;
//import com.sodia.client.GUI.GUIPerfiles;
/**
 * This enum shows all the possible history tokens supported in the application
 */

public enum Place {        
    	
    TRAZABIILIDAD_PROCESOS{               
        Recurso r;
        @Override
        public void setView(Recurso r){
            this.r = r;
        }        
        @Override
        public Recurso getView(){
            return r;
        }
        @Override
        public Widget getContent() {
            return new GUISecond();
        }
    },              	
    USUARIOS{               
        Recurso r;
        @Override
        public void setView(Recurso r){
            this.r = r;
        }        
        @Override
        public Recurso getView(){
            return r;
        }
        @Override
        public Widget getContent() {
            return new GUIUsuarios(r);
        }
    },
    PERFILES{               
        Recurso r;
        @Override
        public void setView(Recurso r){
            this.r = r;
        }        
        @Override
        public Recurso getView(){
            return r;
        }
        @Override
        public Widget getContent() {
            return new GUIPerfiles(r);
        }
    },
    MAESTRO_TIPO_B{               
        Recurso r;
        @Override
        public void setView(Recurso r){
            this.r = r;
        }        
        @Override
        public Recurso getView(){
            return r;
        }
        @Override
        public Widget getContent() {
            return new GUIMaestroTipoB2(r);
        }
    },
    FORMULARIO_DINAMICO {
        Recurso r;

        @Override
        public void setView(Recurso r){
            this.r = r;
        }
        
        @Override
        public Recurso getView(){
            return r;
        }
        
        @Override
        public Widget getContent() {
            return new GUISecond();
        }
    },
    ENTIDAD_DINAMICO {
        Recurso r;

        @Override
        public void setView(Recurso r){
            this.r = r;
        }

        @Override
        public Recurso getView(){
            return r;
        }
        
        @Override
        public Widget getContent() {
            return new GUISecond();
        }
    },
    FLUJO_PROCESO {
        Recurso r;

        @Override
        public void setView(Recurso r){
            this.r = r;
        }
        
        @Override
        public Recurso getView(){
            return r;
        }

        @Override
        public Widget getContent() {
            return new GUISecond();
        }
    },
    inicio {
        Recurso r;
        
        @Override
        public void setView(Recurso r){
            this.r = r;
        }
        
        @Override
        public Recurso getView(){
            return r;
        }

        @Override
        public Widget getContent() {
//            return new BadgesView();
            return new GUISecond();
//			return new MDInicio();
//			return new MaterialDialogs();
//			return new MaterialAutoCompletes();
//                return new WindowView();
//                return new GUIFirst(null);                
//                return new ComboBoxView1();
//                return new GUIMaestroTipoB();
        }
        
    },
    FORMULARIOS{               
        Recurso r;
        @Override
        public void setView(Recurso r){
            this.r = r;
        }        
        @Override
        public Recurso getView(){
            return r;
        }
        @Override
        public Widget getContent() {
            return new GUIFormularios(r);
        }
    };

    public abstract void setView(Recurso r);		
    public abstract Recurso getView();		
    public abstract Widget getContent();
    
}