/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ppp.client.GUI;

//import com.extjs.gxt.ui.client.data.BaseModelData;
import com.frm.proto.client.salpi.AdmUsuariosAplicaLigth;
import com.frm.proto.client.salpi.PUN;
import com.frm.proto.client.RegObjClient;
import com.frm.proto.client.salpi.BaseModelData;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SuggestOracle;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.WidgetCollection;
import com.frm.proto.client.table.dto.User;
import com.frm.proto.client.table.dto.UserOracle;
import com.frm.proto.client.widget.serv.JsonBuilder;
import com.google.gwt.dom.client.Style;
import gwt.material.design.addins.client.autocomplete.MaterialAutoComplete;
import gwt.material.design.addins.client.autocomplete.constants.AutocompleteType;
import gwt.material.design.addins.client.fileuploader.MaterialFileUploader;
import gwt.material.design.addins.client.fileuploader.base.UploadFile;
import gwt.material.design.addins.client.fileuploader.constants.FileMethod;
import gwt.material.design.addins.client.fileuploader.events.SuccessEvent;
import gwt.material.design.client.constants.ButtonSize;
import gwt.material.design.client.constants.ButtonType;
import gwt.material.design.client.constants.Color;
import gwt.material.design.client.constants.IconType;
import gwt.material.design.client.ui.MaterialButton;
import gwt.material.design.client.ui.MaterialCard;
import gwt.material.design.client.ui.MaterialCheckBox;
import gwt.material.design.client.ui.MaterialContainer;
import gwt.material.design.client.ui.MaterialDoubleBox;
import gwt.material.design.client.ui.MaterialFloatBox;
import gwt.material.design.client.ui.MaterialImage;
import gwt.material.design.client.ui.MaterialIntegerBox;
import gwt.material.design.client.ui.MaterialLabel;
import gwt.material.design.client.ui.MaterialLink;
import gwt.material.design.client.ui.MaterialListBox;
import gwt.material.design.client.ui.MaterialListValueBox;
import gwt.material.design.client.ui.MaterialLongBox;
import gwt.material.design.client.ui.MaterialProgress;
import gwt.material.design.client.ui.MaterialRange;
import gwt.material.design.client.ui.MaterialRow;
import gwt.material.design.client.ui.MaterialSwitch;
import gwt.material.design.client.ui.MaterialTextArea;
import gwt.material.design.client.ui.MaterialTextBox;
import gwt.material.design.client.ui.MaterialToast;
import java.util.ArrayList;
import java.util.List;

/**
 *	
 * @author diego
 */
public class GUISecond extends Composite {
    
    private static GUISecondUiBinder uiBinder = GWT.create(GUISecondUiBinder.class);
    
    interface GUISecondUiBinder extends UiBinder<Widget, GUISecond> {
    }
    
//    @UiField MaterialListBox lstOptions;
//    @UiField MaterialCheckBox cbBoxAll, cbBox, cbBlue, cbRed, cbCyan, cbGreen, cbBrown;
//
//    @UiField MaterialSwitch switch1, switch2;
//    @UiField MaterialLabel lblRange;
//    @UiField MaterialRange range;
//
//    @UiField MaterialTextArea txtAreaAuto;
//    @UiField MaterialTextArea txtAreaFocus;
//
//    @UiField MaterialFloatBox txtFloatBox, txtFloatRO;
//    @UiField MaterialIntegerBox txtIntegerBox, txtIntegerRO;
//    @UiField MaterialDoubleBox txtDoubleBox, txtDoubleRO;
//    @UiField MaterialLongBox txtLongBox, txtLongRO;
    
    @UiField MaterialRow mrRoot;
    
//    @UiField MaterialAutoComplete acList;
//    
//    
//     @UiField
//     MaterialFileUploader uploader, uploader2;
//     
//     @UiField
//     MaterialProgress mprogress; 
//     
//     
//     @UiField
//     MaterialCard mcUploader;
//     
//     @UiField
//     MaterialImage imMcUploader;
     
//     @UiField
//     MaterialUploadLabel uploaderLabel;
     
    public GUISecond() {
        initWidget(uiBinder.createAndBindUi(this));        
        mrRoot.setId("asdfvv");
    }
    
}   