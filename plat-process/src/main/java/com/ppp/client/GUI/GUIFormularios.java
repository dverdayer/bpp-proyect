package com.ppp.client.GUI;

import com.frm.proto.client.salpi.EntidadIndepen;
import com.frm.proto.client.FormaGUI;
import com.frm.proto.client.RegObjClient;
import com.frm.proto.client.init.FachaSWGE;
import com.frm.proto.client.init.GridType;
import com.frm.proto.client.salpi.BaseModelData;
import com.frm.proto.client.salpi.HijoBaseModel;
import com.frm.proto.client.salpi.PUN;
import com.frm.proto.client.ui.MaterialDataTableCuston;
import com.frm.proto.client.util.ObjWindow;
import com.frm.proto.client.util.PropsGridPG;
import com.frm.proto.client.util.Recurso;
import com.frm.proto.client.util.SGUI;
import com.frm.proto.client.widget.serv.JsonBuilder;
import com.ppp.client.util.SWGE;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import com.ppp.client.util.FDFormFree;
import gwt.material.design.addins.client.combobox.MaterialComboBox;
import gwt.material.design.addins.client.window.MaterialWindow;
import gwt.material.design.client.constants.Color;
import gwt.material.design.client.constants.IconType;
import gwt.material.design.client.constants.TextAlign;
import gwt.material.design.client.constants.WavesType;
import gwt.material.design.client.ui.MaterialColumn;
import gwt.material.design.client.ui.MaterialContainer;
import gwt.material.design.client.ui.MaterialDropDown;
import gwt.material.design.client.ui.MaterialFAB;
import gwt.material.design.client.ui.MaterialIcon;
import gwt.material.design.client.ui.MaterialLink;
import gwt.material.design.client.ui.MaterialRow;
import gwt.material.design.client.ui.MaterialTextArea;
import gwt.material.design.client.ui.MaterialTextBox;
import java.util.List;


public class GUIFormularios extends Composite implements FormaGUI {
    
    private static FrmUiBinder uiBinder = GWT.create(FrmUiBinder.class);  
    interface FrmUiBinder extends UiBinder<Widget, GUIFormularios> {}
    
    @UiField HTMLPanel gRoot;
    Recurso recurso;
    MaterialFAB mFabDesa = new MaterialFAB();    
    
    public GUIFormularios(Recurso recurso) {                
        initWidget(uiBinder.createAndBindUi(this));        
        this.recurso = recurso;
        this.recurso.setSsEntidad("AdmFormulario");        
    }        

    @Override
    public void onRender() {                                 
        
        MaterialContainer mcPpal = new MaterialContainer();
        mcPpal.setId(PUN.MT_ROOT+PUN.SEPARAY+recurso.getSsControlador()+PUN.SEPARA+recurso.getSsEntidad()+PUN.SEPARA);
        gRoot.add(mcPpal);   
        
            String[] idsGrid = {"kaNlFormulario", "ssActivo", "ssDescripcion"
                    
                    , "admTipoFormulario:kaNlTipoFormulario", "admTipoFormulario:ssDescripcion"
                    , "nlConsecutivo"
            };           
            String[] titulosGrid = {"kaNlFormulario" , "Estructura Terminada", "Descripcion"
                    
                    , "kaNlTipoFormulario", "Tipo Formulario"
                   , "Consecutivo"
            };
            String[] tiposCampo = {PUN.HDD_NU, PUN.CHK_NN, PUN.TXT_NN
                    , PUN.HDD_NU, PUN.CBE_NN                    
                    , PUN.TXN_NN_D0   
            };

            PropsGridPG pgpg = new PropsGridPG(recurso, this.recurso.getSsEntidad(), idsGrid, titulosGrid, tiposCampo);                            
            MaterialDataTableCuston mdt = SWGE.armarGridGENEPageOnly(pgpg, mcPpal, 1);            

            Panel panelTop = mdt.getScaffolding().getTopPanel();
            FachaSWGE.colocarFileUploadMC(panelTop);
            FachaSWGE.colocarFilterMC(panelTop);
                                   
            boolean[] trigger = {false,false,false,false,false,false                       
            };
            boolean[] editables = {true,true,true,true,true,true                    
            };
            
            String[] strGrid = {PUN.SMALL, PUN.SMALL, PUN.LARGE, PUN.SMALL, PUN.SMALL, PUN.SMALL                   
            };
             
            String[] tiposCampoForm = {PUN.HDD_NU, PUN.HDD, PUN.TXT_NN
                    , PUN.HDD_NU, PUN.HDD                    
                    , PUN.HDD_NU   
            };
                      
            MaterialIcon miAdd = new MaterialIcon(IconType.ADD);
            miAdd.setWaves(WavesType.LIGHT);        
            miAdd.setCircle(true);        
            panelTop.add(miAdd);                        
            
            
            miAdd.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {                
                    MaterialIcon macInside = (MaterialIcon)event.getSource();
                    MaterialContainer mcRoot = SGUI.buMCRootParent(macInside);
                    MaterialDataTableCuston mTable = SGUI.buMCTableParent(macInside);   
                    
                    PropsGridPG pG = mTable.getPgpg();
                    
                    
                    ObjWindow om = null;
                    if(RegObjClient.get(PUN.VENTANA_EMER_FILTER+"_"+pG.getRecurso().getSsControlador()+"_"+pG.getStrEntidad()) != null){
                        om = (ObjWindow)RegObjClient.get(PUN.VENTANA_EMER_FILTER+"_"+pG.getRecurso().getSsControlador()+"_"+pG.getStrEntidad());                                        
                    }else{                
                        om = om = SWGE.armarModalMACForm(mcRoot, pG.getRecurso().getSsControlador(), pG.getStrEntidad(), mTable, false);
                        RegObjClient.register(PUN.VENTANA_EMER_FILTER+"_"+pG.getRecurso().getSsControlador()+"_"+pG.getStrEntidad(), om);                    
                    
                        om.getMaterialWindow().setTop(0);
                                                            
                        MaterialRow mrFormDinaPPAL = new MaterialRow();
                        MaterialRow mrForm = SWGE.armarFormPrincipal(recurso.getSsControlador(), recurso.getSsEntidad()
                            , idsGrid, titulosGrid, tiposCampoForm, trigger
                            , editables, strGrid, recurso
                            , null, null);

                        MaterialIcon  mi = new MaterialIcon();
                        mi.setIconType(IconType.MORE_VERT);
                        mi.setIconColor(Color.BLACK);
                        mi.setActivates("dp-4");
                        mi.setWaves(WavesType.DEFAULT);

                        MaterialDropDown mdd = new MaterialDropDown();
                        mdd.setActivator("dp-4");
                        mdd.setConstrainWidth(false);

                        MaterialLink ml1 = new MaterialLink(IconType.FORMAT_ALIGN_CENTER);
                        ml1.setText("Formulario Free");
                        ml1.addClickHandler(new ClickHandler() {
                            @Override
                            public void onClick(ClickEvent event) {  
                                String strEntidad = "AdmFormFree";
                                String strEntidadFree = "AdmPropertiesForm";
                                
                                ObjWindow ow = null;
                                if(RegObjClient.get(PUN.VENTANA_EMER+"_"+pG.getRecurso().getSsControlador()+"_"+strEntidad) != null){
                                    ow = (ObjWindow)RegObjClient.get(PUN.VENTANA_EMER+"_"+pG.getRecurso().getSsControlador()+"_"+strEntidad);                                        
                                }else{                
                                    ow = SWGE.armarModalGeneric(mcRoot, pG.getRecurso().getSsControlador(), strEntidad, false);
                                    RegObjClient.register(PUN.VENTANA_EMER+"_"+pG.getRecurso().getSsControlador()+"_"+strEntidad, ow);
                                    ow.getMaterialWindow().setTop(0);                                                                                                
                                    dibujarVentanaFormFree(recurso.getSsControlador(), ow, new FDFormFree(), strEntidadFree);
                                }
                                ow.getMaterialWindow().open();
                            }
                        });

                        mdd.add(ml1);
                        MaterialLink ml2 = new MaterialLink(IconType.FORMAT_INDENT_DECREASE);
                        ml2.setText("Formulario Entidad");
                        mdd.add(ml2);
                        MaterialLink ml3 = new MaterialLink(IconType.GRID_ON);
                        ml3.setText("Tabla Free");
                        ml3.addClickHandler(new ClickHandler() {
                            @Override
                            public void onClick(ClickEvent event) {  
                                String strEntidad = "AdmTableFree";
                                String strEntidadFree = "AdmPropertiesForm";
                                
                                ObjWindow ow = null;
                                if(RegObjClient.get(PUN.VENTANA_EMER+"_"+pG.getRecurso().getSsControlador()+"_"+strEntidad) != null){
                                    ow = (ObjWindow)RegObjClient.get(PUN.VENTANA_EMER+"_"+pG.getRecurso().getSsControlador()+"_"+strEntidad);                                        
                                }else{                
                                    ow = SWGE.armarModalGeneric(mcRoot, pG.getRecurso().getSsControlador(), strEntidad, false);
                                    RegObjClient.register(PUN.VENTANA_EMER+"_"+pG.getRecurso().getSsControlador()+"_"+strEntidad, ow);
                                    ow.getMaterialWindow().setTop(0);                                                                                                
                                    dibujarVentanaTableFree(recurso.getSsControlador(), ow, new FDFormFree(), strEntidadFree);
                                }
                                ow.getMaterialWindow().open();
                            }
                        });
                        
                        mdd.add(ml3);
                        MaterialLink ml4 = new MaterialLink(IconType.GRID_ON);
                        ml4.setText("Tabla Entidad");
                        mdd.add(ml4);

                        MaterialColumn mcForm = new MaterialColumn();
                        mcForm.setGrid("s11 m11 l6"); 
                        mcForm.add(mrForm);
                        mrFormDinaPPAL.add(mcForm);

                        mcForm = new MaterialColumn();
                        mcForm.setGrid("s1 m1 l4"); 
                        mcForm.add(mi);
                        mcForm.setTextAlign(TextAlign.CENTER);
                        mi.setPaddingTop(15);
                        mcForm.add(mdd);
                        
                        mrFormDinaPPAL.add(mcForm);            
//                        mrFormDinaPPAL.setPaddingTop(80);                                                  
                        om.getMaterialContent().add(mrFormDinaPPAL);                    
                    }                    
                    om.getMaterialWindow().open();
                }
            });
            
    }
    
    public void dibujarVentanaFormFree(String ssControlador, ObjWindow omff 
            , FDFormFree fDFormFree, String strEntidad){
        
        MaterialRow mrForm = SWGE.armarFormPrincipal(recurso.getSsControlador(), strEntidad
                            , new String[]{"txtEtiqueta","chLabelTop"}, new String[]{"Etiqueta","Etiqueta Arriba"}, new String[]{PUN.TXT_NN, PUN.CHK_NN}, new boolean[]{false, false}
                            , new boolean[]{true, true}, new String[]{PUN.MEDIUM, PUN.SMALL}, recurso
                            , null, null);
        
        omff.getMaterialContent().add(mrForm);
        
        
        String[] ids = {"kaNlControlFormulario","ssTitulo","admComponenteFr:kaNlTipoComponente","admComponenteFr:ssDescripcion"
                ,"admControlesGr:kaNlEncRespuesta","admControlesGr:ssDescripcion"
                , "nlOrden", "ssIdhtml", "ssComponente", "ssRequerido"};
        String[] titulos = {"kaNlControlFormulario","Titulo","kaNlTipoComponente","Componente"
                ,"Binding","Control"
                , "nlOrden", "id html", "id Componente", "Requerido"};      
        String[] tipoCampoF = {PUN.HDD_NU,PUN.TXT_XX, PUN.HDD_NU,PUN.CBE_XX
//                ,PUN.HDD_NU,PUN.ENTIDADE_XX
                ,PUN.HDD_NU,PUN.CBE_XX
                , PUN.HDD_NU, PUN.TXT_XX, PUN.TXT_XX, PUN.CHK_NN};
        
        String[] gridCL = {PUN.SMALL, PUN.SMALL, PUN.SMALL, PUN.SMALL
                , PUN.SMALL, PUN.SMALL
                , PUN.SMALL, PUN.SMALL, PUN.SMALL, PUN.SMALL
        };
        
        BaseModelData paretosBorrar = new BaseModelData();

        paretosBorrar.set("admComponenteFr:ssDescripcion", "admControlesGr:ssDescripcion");
        boolean[] vtEditables = {true, true, true, true, true, true, true, false, true, true};
        boolean[] trigger1 = {false,false,false,true,false,false,false, false, false, false, false};
        
        PropsGridPG pgpg1 = new PropsGridPG(recurso, strEntidad, ids, titulos, tipoCampoF);                                            
        pgpg1.setGridCL(gridCL);
        pgpg1.setEditables(vtEditables);
        pgpg1.setTrigger(trigger1);
        
        MaterialDataTableCuston mdt2 = SWGE.armarGridGENE(pgpg1, omff.getMaterialContent(), 280, GridType.SIMPLE_FORM, 0); 
        mdt2.getTableTitle().setText("");
    }
    
    public void dibujarVentanaTableFree(String ssControlador, ObjWindow omff 
            , FDFormFree fDFormFree, String strEntidad){
        
        MaterialRow mrForm = SWGE.armarFormPrincipal(recurso.getSsControlador(), strEntidad
                            , new String[]{"txtEtiqueta"}, new String[]{"Etiqueta"}, new String[]{PUN.TXT_NN}, new boolean[]{false}
                            , new boolean[]{true}, new String[]{PUN.LARGE}, recurso
                            , null, null);
        
        omff.getMaterialContent().add(mrForm);
        
        String[] ids = {"kaNlControlFormulario", "ssTitulo" ,"admComponenteGr:kaNlTipoComponente" ,"admComponenteGr:ssDescripcion"
                , "ssCalculado"
                , "admControlesGr:kaNlEncRespuesta","admControlesGr:ssDescripcion"
                , "nlOrden", "ssIdhtml", "ssComponente", "ssRequerido"};
        String[] titulos = {"kaNlControlFormulario", "Titulo", "kaNlTipoComponente", "Componente"
                ,"Campo Calculado"
                ,"kaNlEncRespuesta", "Control"
                , "nlOrden", "id html", "id Componente", "Requerido"};
        boolean[] vtEditables = {true, true, true, true, false, true, false, true, false, true, true};
        boolean[] trigger1 = {false, false, false, true, false, false, false, false, false, false, false};
        String[] tipoCampoF = {PUN.HDD_NU, PUN.TXT_XX, PUN.HDD_NU, PUN.CBE_XX
                , PUN.TXT_XX
//                , PUN.HDD_NU, PUN.ENTIDADE_XX
                , PUN.HDD_NU, PUN.CBE_XX                
                , PUN.HDD_NU, PUN.TXT_XX, PUN.TXT_XX, PUN.CHK_NN};
        String[] gridCL = {PUN.SMALL, PUN.SMALL, PUN.SMALL, PUN.SMALL
                , PUN.SMALL
                , PUN.SMALL, PUN.SMALL
                , PUN.SMALL, PUN.SMALL, PUN.SMALL, PUN.SMALL
        };
        
        PropsGridPG pgpg1 = new PropsGridPG(recurso, strEntidad, ids, titulos, tipoCampoF);                                            
        pgpg1.setGridCL(gridCL);
        pgpg1.setEditables(vtEditables);
        pgpg1.setTrigger(trigger1);
        
        MaterialDataTableCuston mdt2 = SWGE.armarGridGENE(pgpg1, omff.getMaterialContent(), 280, GridType.SIMPLE_FORM, 0); 
        mdt2.getTableTitle().setText("");
    }
    
    
    
    
    @Override
    public boolean validatorBeforeSave(String strEntidad, BaseModelData bmdFP, List<HijoBaseModel> lstHijos, List<EntidadIndepen> lstDeleteIndepen) {
        return true;
    }

    @Override
    public void getTrigger(Object c, String strIdControl, String strAction) {
        
        if(strAction.equals("OnSelectForm") && c instanceof MaterialComboBox
                && strIdControl.equals("admComponenteFr_ssDescripcion")){
            MaterialComboBox mco = (MaterialComboBox)c;            
            MaterialRow mr = SGUI.getFormPanelByComponente(mco);            
            Widget w = SGUI.buWidgetChild(mr, "admControlesGr_ssDescripcion");            
            MaterialComboBox mco2 = (MaterialComboBox)w;            
            mco2.close();
            mco2.clear();
            if(mco.getSelectedValue() != null && !mco.getSelectedValue().isEmpty()){                               
                JsonBuilder.refreshCBE("AdmPropertiesForm", "admControlesGr:ssDescripcion", mr,  mco2);                        
            }
        }else if(strAction.equals("OnSelectForm") && c instanceof MaterialComboBox
                && strIdControl.equals("admComponenteGr_ssDescripcion")){
            MaterialComboBox mco = (MaterialComboBox)c;            
            MaterialRow mr = SGUI.getFormPanelByComponente(mco);    
            MaterialComboBox mco2 = (MaterialComboBox)SGUI.buWidgetChild(mr, "admControlesGr_ssDescripcion");
            MaterialTextBox mcoCal = (MaterialTextBox)SGUI.buWidgetChild(mr, "ssCalculado");            
            mco2.close();
            mco2.clear();
            mcoCal.clear();
            if(mco.getSelectedValue() != null && !mco.getSelectedValue().isEmpty()){               
                BaseModelData bmdSelect = (BaseModelData)mco.getSingleValue();                                                                                           
                if(bmdSelect.get("ssDescripcion").equals(PUN.TEXT_CALCULADO) ||
                        bmdSelect.get("ssDescripcion").equals(PUN.TEXT_CALCULADO_MONEDA)){                                          
                    MaterialContainer mcRoot = SGUI.buMCRootParent(mco);
                    mcoCal.addClickHandler(new ClickHandler() {
                        @Override
                        public void onClick(ClickEvent event) {
                            armarVentanaCalculado(mcRoot);
                        }
                    });                    
                    mco2.setEnabled(false);                     
                    mcoCal.setEnabled(true);  
                    mcoCal.setLength(0);
                }else{                
                    mco2.setEnabled(true);  
                    mcoCal.setEnabled(false); 
                    JsonBuilder.refreshCBE("AdmPropertiesForm", "admControlesGr:ssDescripcion", mr,  mco2);                        
                }
            }
        }        
    }
    
    public void armarVentanaCalculado(MaterialContainer mcRoot){
        String strEntidad = "campoCalculado";        
        ObjWindow ow = null;
        ow = SWGE.armarModalGeneric(mcRoot, getControlador(), strEntidad, false);
        ow.getMaterialWindow().setTitle("Helper Calculo");
        RegObjClient.register(PUN.VENTANA_EMER+"_"+getControlador()+"_"+strEntidad, ow);
        ow.getMaterialWindow().setTop(0); 
        
        MaterialRow mr = new MaterialRow();
        ow.getMaterialContent().add(mr);
        MaterialColumn mc = new MaterialColumn();
        mc.setGrid(PUN.LARGE);
        mr.add(mc);
        MaterialTextArea mta = new MaterialTextArea();                
        mta.setLabel("Campo Calculado");
        mc.add(mta); 
        
        ow.getMaterialWindow().open();
    }

    @Override
    public String getEntidad() {
        return recurso.getSsEntidad();
    }

    @Override
    public String getControlador() {
        return recurso.getSsControlador();
    }

    @Override
    public int getNlPermisos() {
        return recurso.getNlPermisos();
    }

    @Override
    public String getStrXML() {
        return recurso.getSsXml();
    }
    
    public static MaterialDataTableCuston dibujarGridMaestroTipoB(PropsGridPG pgpg, MaterialContainer mcPpal){                                        
        MaterialDataTableCuston mdt = com.frm.proto.client.util.SWGE.armarGridPageMaeTipoB(pgpg, mcPpal);                                       
        
        Panel panelTop1 = mdt.getScaffolding().getToolPanel();        
        FachaSWGE.colocarFileUploadMC(panelTop1);
        FachaSWGE.colocarFilterMC(panelTop1);
        FachaSWGE.colocarAddMC(panelTop1);
        FachaSWGE.colocarSelectFilaMC(mdt);
        
        return mdt;
    }

}
