package com.ppp.client.GUI;

import com.frm.proto.client.salpi.EntidadIndepen;
import com.frm.proto.client.FormaGUI;
import com.frm.proto.client.GUI;
import com.frm.proto.client.salpi.BaseModelData;
import com.frm.proto.client.salpi.HijoBaseModel;
import com.frm.proto.client.salpi.PUN;
import com.frm.proto.client.salpi.Pareto;
import com.frm.proto.client.salpi.VentanaLoad;
import com.frm.proto.client.ui.MaterialDataTableCuston;
import com.frm.proto.client.util.LsMethod;
import com.frm.proto.client.util.PropsGridPG;
import com.frm.proto.client.util.Recurso;
import com.ppp.client.util.SWGE;    
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.frm.proto.client.init.GridType;
import gwt.material.design.client.constants.ButtonSize;
import gwt.material.design.client.constants.ButtonType;
import gwt.material.design.client.constants.Color;
import gwt.material.design.client.constants.IconSize;
import gwt.material.design.client.constants.IconType;
import gwt.material.design.client.ui.MaterialButton;
import gwt.material.design.client.ui.MaterialContainer;
import gwt.material.design.client.ui.MaterialFAB;
import gwt.material.design.client.ui.MaterialRow;
import gwt.material.design.client.ui.MaterialTab;
import java.util.List;


public class GUIUsuarios extends Composite implements FormaGUI {
    
    private static FrmUiBinder uiBinder = GWT.create(FrmUiBinder.class);  
    interface FrmUiBinder extends UiBinder<Widget, GUIUsuarios> {}
    
    @UiField HTMLPanel gRoot;
    MaterialTab mtabsHeader;
    Recurso recurso;
    
    MaterialFAB mFab = new MaterialFAB();    
//    MaterialFAB mFabDesa = new MaterialFAB();    
    
    public GUIUsuarios(Recurso recurso) {                
        initWidget(uiBinder.createAndBindUi(this));        
        this.recurso = recurso;
        this.recurso.setSsEntidad("AdmUsuarios");        
    }        

    @Override
    public void onRender() {                                 
        mtabsHeader = SWGE.getTabsMG(recurso, gRoot);    
        MaterialContainer mrPest = SWGE.getPestCT(recurso, gRoot);
        
        MaterialRow mrLista = SWGE.getItemPestMG(recurso, mtabsHeader, mrPest, "lista", "Lista", true);  
        
            String[] idsGrid = {"kaNlUsuario","ssLogin","ssClave", "ssBloqueo"
                           , "admTiposUsuario:kaNlTiposUsuario","admTiposUsuario:ssDescripcion"
                           , "crTerceros:kaNl","crTerceros:ssPrimerNombre"
                           , "crTerceros:ssSegundoNombre","crTerceros:ssPrimerApellido"
                           , "crTerceros:ssSegundoApellido","crTerceros:ssCedula"
                           , "crTerceros:ssCelular","crTerceros:ssEmail"
            };           
            String[] titulosGrid = {"kaNlUsuario","Login","Password","Bloqueado"
                           , "admTU:kaNl","Tipo"
                           , "CrTerceros:kaNl","Nombre Tercero"
                           , "Segundo Nombre","Primer Apellido"
                           , "Segundo Apellido","Cedula"
                           , "Celular","E-mail"
            };
            String[] tiposCampoGrid = {PUN.HDD_NU,PUN.TXT_NN,PUN.TXT_NN,PUN.CHK_NN
                       ,PUN.HDD_NU, PUN.PPP_XX
                       ,PUN.HDD_NU, PUN.TXT_NN
                       ,PUN.TXT_XX, PUN.TXT_NN
                       ,PUN.TXT_XX, PUN.TXT_NN
                       ,PUN.TXT_XX, PUN.PPP_XX
            };
            PropsGridPG pgpg = new PropsGridPG(recurso, "AdmUsuarios", idsGrid, titulosGrid, tiposCampoGrid);                            
            MaterialDataTableCuston mdt = SWGE.armarGridGENEPage(pgpg, mrLista, 1);            
//            mdt.getTableTitle().setText("My Custom Table");
            mdt.getTableTitle().setText("");
            
        mtabsHeader.selectTab(PUN._ITEM_TAB_BODY+recurso.getSsControlador()+PUN.SEPARA+recurso.getSsEntidad()+PUN.SEPARA+"Lista");            
        
        
        MaterialRow mrForma = SWGE.getItemPestMG(recurso, mtabsHeader, mrPest, "Detalle", "FORMA-GRID", true);                       
                              
            String[] ids = {"kaNlUsuario", "ssLogin","ssClave", "ssBloqueo"
                    , "crTerceros:kaNl", "crTerceros:ssPrimerNombre"
                    , "admTiposUsuario:kaNlTiposUsuario","admTiposUsuario:ssDescripcion"
            };
            String[] titulos = {"kaNlUsuario", "Login","Password","Bloqueado"
                     , "Tercero:kaNl", "Tercero"
                     , "admTiposUsuario:kaNlTiposUsuario","Tipo Tercero"
            };
            String[] tiposCampo = {PUN.HDD_NU, PUN.TXT_NN, PUN.TXT_NN, PUN.CHK_NN
//                    , PUN.HDD_NU, PUN.PPP_NN
                    , PUN.HDD_NU, PUN.ENTIDAD_NN
                    , PUN.HDD_NU, PUN.CBE_NN
            };
            String[] strGrid = {PUN.SMALL, PUN.SMALL,PUN.SMALL,PUN.SMALL
                    ,PUN.SMALL,PUN.SMALL
                    ,PUN.SMALL,PUN.SMALL
            };        
            boolean[] trigger = {false,false,false,false
                        ,false,false
                        ,false,false
            };
            boolean[] editables = {true,true,true,true
                    ,true,true
                    ,true,true
            };

//            MaterialRow mrForm = SWGE.armarFormHijo(recurso.getSsControlador(), recurso.getSsEntidad()
            MaterialRow mrForm = SWGE.armarFormPrincipal(recurso.getSsControlador(), recurso.getSsEntidad()
                , ids, titulos, tiposCampo, trigger
                , editables, strGrid, recurso
                , null, null);  
        
        mrForma.add(mrForm);        
                                       
        String[] idsGrid1 = {"kaNlPerfilesUsuario","admPerfiles:kaNlPerfil","admPerfiles:ssPerfil","ssBloqueado"};
        String[] titulosGrid1 = {"....","....","Descripcion","Bloqueado"};
        String[] tiposCampoGrid1 = {PUN.HDD_NU, PUN.TXT_NN, PUN.CBE_XX, PUN.CHK_NN};                   
        String[] gridCL = {PUN.SMALL, PUN.SMALL,PUN.SMALL,PUN.SMALL};                                   
        boolean[] editables1 = {true,true,true,true};                    
        boolean[] trigger1 = {true,true,true,true};                    
   
        PropsGridPG pgpg1 = new PropsGridPG(recurso, "AdmPerfilesUsuario", idsGrid1, titulosGrid1, tiposCampoGrid1);                                            
        pgpg1.setGridCL(gridCL);
        pgpg1.setEditables(editables1);
        pgpg1.setTrigger(trigger1);
        
//        int height1 = Window.getClientHeight()-632;
        MaterialDataTableCuston mdt2 = SWGE.armarGridGENE(pgpg1, mrForma, 280, GridType.SIMPLE_FORM, 0); 
        mdt2.getTableTitle().setText("");
        
        MaterialButton mbActios = new MaterialButton(ButtonType.FLOATING);                
//        mbActios.setBackgroundColor(Color.BLUE_DARKEN_4);
        mbActios.setBackgroundColor(Color.RED_ACCENT_2);
        mbActios.setSize(ButtonSize.LARGE);
        mbActios.setIconType(IconType.DONE);
        mbActios.setIconSize(IconSize.LARGE);
        
        mFab = new MaterialFAB();
        mFab.add(mbActios);
        mrPest.add(mFab);
        
        mbActios.addClickHandler(LsMethod.grabarTrans(mtabsHeader, mFab, recurso));
               
        mFab.setVisible(false);

    }
    
    @Override
    public boolean validatorBeforeSave(String strEntidad, BaseModelData bmdFP, List<HijoBaseModel> lstHijos, List<EntidadIndepen> lstDeleteIndepen) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return true;
    }

    @Override
    public void getTrigger(Object c, String strIdControl, String strAction) {
          GUI.valueNavBar(strAction, mtabsHeader, mFab);  
//          BaseModelData bmd = (BaseModelData)c;
//          bmd.set("strTab", "12");
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        
    }

    @Override
    public String getEntidad() {
        return recurso.getSsEntidad();
    }

    @Override
    public String getControlador() {
        return recurso.getSsControlador();
    }

    @Override
    public int getNlPermisos() {
        return recurso.getNlPermisos();
    }

    @Override
    public String getStrXML() {
        return recurso.getSsXml();
    }
    
    public static VentanaLoad getVLTiposUsuario(String ssControlador){
        String[] idsRecur = {"kaNlTiposUsuario","ssDescripcion"};
        String[] filter = {"ssDescripcion"};
        String[] titulosRecur = {"....","Descripcion"};
        String[] tipoCampoRecur = {PUN.HDD_NU,PUN.TXT_NN};       
        
        Pareto[] vtPareto = {new Pareto("admTiposUsuario:kaNlTiposUsuario", "kaNlTiposUsuario")
                             ,new Pareto("admTiposUsuario:ssDescripcion", "ssDescripcion")};
        
        VentanaLoad vl = new VentanaLoad("admTiposUsuario", ssControlador, idsRecur
                , titulosRecur, tipoCampoRecur, filter,vtPareto);
        return vl;
    }
}
