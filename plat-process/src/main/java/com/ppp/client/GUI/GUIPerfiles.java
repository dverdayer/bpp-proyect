package com.ppp.client.GUI;

import com.frm.proto.client.salpi.EntidadIndepen;
import com.frm.proto.client.FormaGUI;
import com.frm.proto.client.GUI;
import com.frm.proto.client.salpi.BaseModelData;
import com.frm.proto.client.salpi.HijoBaseModel;
import com.frm.proto.client.salpi.PUN;
import com.frm.proto.client.salpi.Pareto;
import com.frm.proto.client.salpi.VentanaLoad;
import com.frm.proto.client.ui.MaterialDataTableCuston;
import com.frm.proto.client.util.PropsGridPG;
import com.frm.proto.client.util.Recurso;
import com.ppp.client.util.SWGE;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.frm.proto.client.init.GridType;
import gwt.material.design.client.ui.MaterialContainer;
import gwt.material.design.client.ui.MaterialFAB;
import gwt.material.design.client.ui.MaterialRow;
import gwt.material.design.client.ui.MaterialTab;
import java.util.List;


public class GUIPerfiles extends Composite implements FormaGUI {
    
    private static FrmUiBinder uiBinder = GWT.create(FrmUiBinder.class);  
    interface FrmUiBinder extends UiBinder<Widget, GUIPerfiles> {}
    
    @UiField HTMLPanel gRoot;
    MaterialTab mtabsHeader;
    Recurso recurso;
    
    MaterialFAB mFab = new MaterialFAB();    
//    MaterialFAB mFabDesa = new MaterialFAB();    
    
    public GUIPerfiles(Recurso recurso) {                
        initWidget(uiBinder.createAndBindUi(this));        
        this.recurso = recurso;
        this.recurso.setSsEntidad("AdmPerfiles");        
    }        

    @Override
    public void onRender() {                                 
        mtabsHeader = SWGE.getTabsMG(recurso, gRoot);    
        MaterialContainer mrPest = SWGE.getPestCT(recurso, gRoot);
        
        MaterialRow mrLista = SWGE.getItemPestMG(recurso, mtabsHeader, mrPest, "lista", "Lista", true);  
        
            String[] idsGrid = {"kaNlPerfil","ssPerfil","ssActivo","ssBorrado"
            };           
            String[] titulosGrid = {"....","Descripcion","Activo","Borrado"
            };
            String[] tiposCampo = {PUN.HDD_NU,PUN.TXT_NN,PUN.CHK_NN,PUN.CHK_NN                       
            };
            PropsGridPG pgpg = new PropsGridPG(recurso, "AdmPerfiles", idsGrid, titulosGrid, tiposCampo);                            
            MaterialDataTableCuston mdt = SWGE.armarGridGENEPage(pgpg, mrLista, 1);            
            mdt.getTableTitle().setText("");
            
        mtabsHeader.selectTab(PUN._ITEM_TAB_BODY+recurso.getSsControlador()+PUN.SEPARA+recurso.getSsEntidad()+PUN.SEPARA+"Lista");            
        
        
        MaterialRow mrForma = SWGE.getItemPestMG(recurso, mtabsHeader, mrPest, "Detalle", "FORMA-GRID", true);                       
                              
            String[] ids = {"kaNlPerfil","ssPerfil","ssActivo","ssBorrado"
            };
            String[] titulos = {"....","Descripcion","Activo","Borrado"
            };
           
            String[] strGrid = {PUN.SMALL, PUN.SMALL,PUN.SMALL,PUN.SMALL                   
            };        
            boolean[] trigger = {false,false,false,false                       
            };
            boolean[] editables = {true,true,true,true                    
            };

//            MaterialRow mrForm = SWGE.armarFormHijo(recurso.getSsControlador(), recurso.getSsEntidad()
            MaterialRow mrForm = SWGE.armarFormPrincipal(recurso.getSsControlador(), recurso.getSsEntidad()
                , ids, titulos, tiposCampo, trigger
                , editables, strGrid, recurso
                , null, null);
            
//            mrForm.add(new MaterialCameraCapture());
        
        mrForma.add(mrForm);        
                                       
        String[] idsGrid1 = {"kaNlPerfilesRecurso","admRecursos:kaNlRecurso","admRecursos:ssRecurso",
                         "admRecursos:admModulos:kaNlModulo", "admRecursos:admModulos:ssDescripcion"
                         ,"ssBloqueado","ssCreate","ssUpdate","ssDelete"};
        String[] titulosGrid1 = {"...","...","Recurso"
                    ,"...","Modulo"
                    , "Activo","Crear","Modificar","eliminar"};
        String[] tiposCampoGrid1 = {PUN.HDD_NU,PUN.HDD_NU, PUN.TXT_NN
                ,PUN.HDD_NU, PUN.TXT_NN
                , PUN.CHK_NN, PUN.CHK_NN, PUN.CHK_NN, PUN.CHK_NN
        };                   
        String[] gridCL = {PUN.SMALL, PUN.SMALL,PUN.SMALL
                , PUN.SMALL, PUN.SMALL
                , PUN.SMALL, PUN.SMALL, PUN.SMALL, PUN.SMALL
        };                                   
        boolean[] editables1 = {false,false,false,false,false,true,true,true,true};                    
        boolean[] trigger1 = {false,false,false,false,false, true,true,true,true};                    
   
        PropsGridPG pgpg1 = new PropsGridPG(recurso, "AdmPerfilesRecursos", idsGrid1, titulosGrid1, tiposCampoGrid1);                                            
        pgpg1.setGridCL(gridCL);
        pgpg1.setEditables(editables1);
        pgpg1.setTrigger(trigger1);
        
        BaseModelData bmdVentanaLoad = new BaseModelData(); 
        bmdVentanaLoad.set(pgpg1.getStrEntidad(), getVLTiposUsuario(pgpg1.getRecurso().getSsControlador()));
        pgpg1.setBmdVentanaLoad(bmdVentanaLoad);
                
        MaterialDataTableCuston mdt2 = SWGE.armarGridGENE(pgpg1, mrForma, 350, GridType.SIMPLE_GRID_TO_GRID, 0); 
//        MaterialDataTableCuston mdt2 = SWGE.armarGridGENE(pgpg1, mrForma, height1, GridType.SIMPLE_FORM, 0); 
        mdt2.getTableTitle().setText("");
        
        mFab = new MaterialFAB();
        SWGE.armarButtonGrabar(mtabsHeader, mFab, recurso, mrPest);

    }
    
    @Override
    public boolean validatorBeforeSave(String strEntidad, BaseModelData bmdFP, List<HijoBaseModel> lstHijos, List<EntidadIndepen> lstDeleteIndepen) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return true;
    }

    @Override
    public void getTrigger(Object c, String strIdControl, String strAction) {
          GUI.valueNavBar(strAction, mtabsHeader, mFab);  
//          BaseModelData bmd = (BaseModelData)c;
//          bmd.set("strTab", "12");
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        
    }

    @Override
    public String getEntidad() {
        return recurso.getSsEntidad();
    }

    @Override
    public String getControlador() {
        return recurso.getSsControlador();
    }

    @Override
    public int getNlPermisos() {
        return recurso.getNlPermisos();
    }

    @Override
    public String getStrXML() {
        return recurso.getSsXml();
    }
    
    public static VentanaLoad getVLTiposUsuario(String ssControlador){
        String[] idsRecur = {"kaNlRecurso","ssRecurso","nlOrden","admRecursos:kaNlRecurso","admRecursos:ssRecurso"
            ,"ssXml","ssVariable","ssNombre","ssTipoVentana"
            ,"admModulos:kaNlModulo","admModulos:ssDescripcion","ssBorrado"};

        String[] filter = {"ssRecurso","ssXml","ssVariable","ssNombre","admModulos:kaNlModulo","admModulos:ssDescripcion"};

        String[] titulosRecur = {"....","Descripcion","Orden","...","Padre"
                            ,"XML","variable","Nombre","Tipo Ventana","...","Aplicacion","Borrado"};
        
        String[] tipoCampoRecur = {PUN.HDD_NU, PUN.TXT_NN, PUN.TXN_NN_D0
                    ,PUN.HDD_NU, PUN.ENTIDADE_XX, PUN.TXT_NN, PUN.TXT_NN, PUN.TXT_NN
                    ,PUN.TXT_NN, PUN.HDD_NU, PUN.CBE_NN, PUN.CHK_NN
            };            
            Pareto[] vtPareto = {new Pareto("admRecursos:kaNlRecurso", "kaNlRecurso")
                                 ,new Pareto("admRecursos:ssRecurso", "ssRecurso")
                                 ,new Pareto("admRecursos:admModulos:kaNlModulo", "admModulos:kaNlModulo")
                                 ,new Pareto("admRecursos:admModulos:ssDescripcion", "admModulos:ssDescripcion")
            };
                    
            VentanaLoad vl = new VentanaLoad("AdmRecursos", ssControlador, idsRecur
                , titulosRecur, tipoCampoRecur, filter,vtPareto);
        return vl;                     
    }
}
