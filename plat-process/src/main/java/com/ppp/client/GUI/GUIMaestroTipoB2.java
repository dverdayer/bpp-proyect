/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ppp.client.GUI;



/**
 *
 * @author diego
 */

//import com.extjs.gxt.ui.client.data.BaseModelData;
import com.frm.proto.client.FormaGUI;
import com.frm.proto.client.RegObjClient;
import com.frm.proto.client.salpi.BaseModelData;
import com.frm.proto.client.salpi.EntidadIndepen;
import com.frm.proto.client.salpi.HijoBaseModel;
import com.frm.proto.client.salpi.PUN;
import com.frm.proto.client.ui.MaterialDataTableCuston;
import com.frm.proto.client.util.PropsGridPG;
import com.frm.proto.client.util.Recurso;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.ppp.client.PropsGridPGs;
import com.frm.proto.client.init.FachaSWGE;
import com.frm.proto.client.util.ObjWindow;
import com.frm.proto.client.util.SWGE;
import gwt.material.design.client.ui.MaterialContainer;
import gwt.material.design.client.ui.MaterialRow;
import gwt.material.design.client.ui.MaterialTab;
import java.util.List;


public class GUIMaestroTipoB2 extends Composite implements FormaGUI {
    
    private static GUIMaestroTipo2Binder uiBinder = GWT.create(GUIMaestroTipo2Binder.class);  
    interface GUIMaestroTipo2Binder extends UiBinder<Widget, GUIMaestroTipoB2> {}
    
    @UiField HTMLPanel gRoot;  
    MaterialTab mtabsHeader;
    Recurso recurso;
    
    
    public GUIMaestroTipoB2(Recurso recurso) {                
        initWidget(uiBinder.createAndBindUi(this));        
        this.recurso = recurso;        
    }        

    @Override
    public void onRender() {                                 
                       
        MaterialContainer mcPpal = new MaterialContainer();
        mcPpal.setId(PUN.MT_ROOT+PUN.SEPARAY+recurso.getSsControlador()+PUN.SEPARA+recurso.getSsEntidad()+PUN.SEPARA);
        gRoot.add(mcPpal);                            
        
        PropsGridPG pG = PropsGridPGs.getPropsGridPG(this.recurso);
        
        MaterialDataTableCuston mdt = FachaSWGE.dibujarGridMaestroTipoB(pG, mcPpal);
        
        ObjWindow om = SWGE.armarModalMACForm(mcPpal, pG.getRecurso().getSsControlador(), pG.getStrEntidad(), mdt, false);   
        RegObjClient.register(PUN.VENTANA_EMER+"_"+pG.getRecurso().getSsControlador()+"_"+pG.getStrEntidad(), om);
        MaterialRow mrForm = SWGE.armarFormHijoModal(pG.getRecurso().getSsControlador(), pG.getStrEntidad()
            , pG.getIds(), pG.getTitulos()
            , pG.getTiposCampos(),pG.getTrigger()
            , pG.getEditables(), pG.getGridCL(), pG.getRecurso()
            , pG.getBmdVentanaLoad(), pG.getBmdPB()
            , false);                                
        om.getMaterialContent().add(mrForm);   
    }
    
    @Override
    public boolean validatorBeforeSave(String strEntidad, BaseModelData bmdFP, List<HijoBaseModel> lstHijos, List<EntidadIndepen> lstDeleteIndepen) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void getTrigger(Object c, String strIdControl, String strAction) {
//          GUI.valueNavBar(strAction, mtabsHeader);  
//          BaseModelData bmd = (BaseModelData)c;
//          bmd.set("strTab", "12");
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        
    }

    @Override
    public String getEntidad() {
        return recurso.getSsEntidad();
    }

    @Override
    public String getControlador() {
        return recurso.getSsControlador();
    }

    @Override
    public int getNlPermisos() {
        return recurso.getNlPermisos();
    }

    @Override
    public String getStrXML() {
        return recurso.getSsXml();
    }        
}
