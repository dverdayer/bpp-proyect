/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ppp.client;

import com.frm.proto.client.salpi.BaseModelData;
//import com.extjs.gxt.ui.client.data.BaseModelData;
import com.frm.proto.client.salpi.MsgFinalUser;
import com.frm.proto.client.salpi.PUN;
import com.frm.proto.client.RegObjClient;
import com.frm.proto.client.init.SERVICES;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Main entry point.
 *
 * @author diego
 */
public class MainEntryPoint implements EntryPoint {

    /**
     * Creates a new instance of MainEntryPoint
     */
    public MainEntryPoint() {
    }

    /**
     * The entry point method, called automatically by loading a module that
     * declares an implementing class as an entry-point
     */
    public void onModuleLoad() {        
        SERVICES.getService().getSessionActiva(new AsyncCallback<MsgFinalUser>() {
            @Override
            public void onFailure(Throwable caught) {
                //MessageBox.alert("Error", "No pudo cargar la pagina principal Cierre Session" , null);
                RootPanel.get().add(new MaterialDemo());
            }
            @Override
            public void onSuccess(final MsgFinalUser mfu) {
                if(mfu != null && mfu.isExito()){                                       
                    BaseModelData bmdAR = mfu.getBmd();
                    if(bmdAR.get(PUN.STR_USER_ACTUAL) != null){                        
                        RegObjClient.register(PUN.STR_USER_ACTUAL, bmdAR.get(PUN.STR_USER_ACTUAL));
                        RegObjClient.register(PUN.APLU, bmdAR.get(PUN.STR_USER_ACTUAL));
                        RegObjClient.register(PUN.CALL_BACK, 0);                        
                        RootPanel.get().add(new MaterialDemo());
//                        RootPanel.get().add(new MaterialDemo1());
                    }else{                        
//                        MessageBox.alert("Session Usuario", "Error dentro de los datos no viene la session del usuario", null);
//                            com.google.gwt.user.client.Window.Location.assign(mfu.getParamvalue0());
                    }                   
                }else{
                    com.google.gwt.user.client.Window.Location.assign(mfu.getParamvalue0());
                }
            }
        });
        
//        RootPanel.get().add(new MaterialDemo());
//        RootPanel.get().add(new NavBarView());
    }
}
